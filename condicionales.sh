#!/bin/bash
#
# Comparación de cadenas alfanuméricas
#

#CADENA1=uno"
#CADENA2=dos"
#CADENA3="

read -p "introduzca tres cadenas" CADENA1 CADENA2 CADENA3
#es interesante separar una pregunta una variable
if [ $CADENA1 = $CADENA2 ]; then
    echo "\$CADENA1 es igual a \$CADENA2"


elif [ $CADENA1 != $CADENA2 ]; then
    echo "\$CADENA1 no es igual a \$CADENA2"

fi

if [ -z $CADENA3 ]; then
    echo "\$CADENA3 esta vacia"
fi
